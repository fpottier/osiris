(**[defs ds] transforms the Coq definitions [ds] into a PPrint document. *)
val defs : Coq.defs -> PPrint.document
