From osiris Require Import base.
From osiris.lang Require Import locations lang.
From osiris.semantics Require Import code eval simplification.
From osiris.program_logic.pure Require Export wp judgements.

(** This file defines basic reasoning rules over [pure] judgements. *)

Section pure_rules.

  (** *Basic reasoning rules for [pure]. *)

  (* A reasoning rule for [ret]. *)

  (* The subgoal [v = #a] is explicitly isolated so as to make this lemma
    more widely applicable. A subgoal of the form [v = #a], where [a] is
    a Coq metavariable, can be solved by the tactic [encode]. *)

  Lemma pure_ret `{Observe A V} {E}
    (φ : A → Prop) ψ (v : V) a :
    v = ♯ a →
    φ a →
    pure (E := E) (ret v) φ ψ.
  Proof.
    intros; apply pure_wp_ret; esplit; eauto.
  Qed.

  (* A reasoning rule for ret that can instantiate the goal when it is an evar *)

  Lemma pure_ret_eq `{Observe A V} {E}
    (a : A) ψ :
    pure (E := E) (V := V) (ret ♯a) (λ a', a' = a) ψ.
  Proof.
    intros; eapply pure_ret; eauto.
  Qed.

  Lemma pure_returns `{Observe A V} {E}
    (a : val) (k : val -> micro V E) (φ : A -> _) ψ:
    pure (V := V) (k a) φ ψ ->
    returns (V := val) (λ v , pure (k v) φ ψ) a.
  Proof.
    intros; eauto with pure.
  Qed.

  Lemma pure_throw `{Observe A V} {E}
    (φ : A -> _) (ψ : _ -> Prop) e :
    ψ e →
    pure (E := E) (V := V) (throw e) φ ψ.
  Proof.
    intros; by apply pure_wp_throw.
  Qed.

  (* The consequence rule. *)

  Lemma pure_mono `{Observe A V} {E}
    m (φ φ' : A -> Prop) (ψ ψ' : _ -> Prop) :
    pure (E := E) m φ ψ →
    (∀ a, φ a → φ' a) →
    (∀ a, ψ a → ψ' a) →
    pure (V := V) m φ' ψ'.
  Proof.
    intros; eapply pure_wp_mono; [ eauto | |]; firstorder.
  Qed.

  Lemma pure_ret_mono `{Observe A V} {E}
    m (φ φ' : A -> Prop) (ψ : _ -> Prop) :
    pure (E := E) m φ ψ →
    (∀ a, φ a → φ' a) →
    pure (V := V) m φ' ψ.
  Proof.
    intros; eapply pure_mono ; eauto.
  Qed.

  Lemma pure_exn_mono `{Observe A V} {E}
    m (φ : A -> Prop) (ψ ψ' : _ -> Prop) :
    pure (E := E) m φ ψ →
    (∀ a, ψ a → ψ' a) →
    pure (V := V) m φ ψ'.
  Proof.
    intros; eapply pure_mono ; eauto.
  Qed.

  Lemma pure_simp `{Observe A V} {E}
    (φ : A -> Prop) ψ m m' :
    simp m m' →
    pure m' φ ψ →
    pure (E := E) (V := V) m φ ψ.
  Proof.
    by eapply pure_wp_simp.
  Qed.

  (* A reasoning rule for [try2]. *)

  (* The rule is degenerate; [m] is not allowed to reduce to [throw _],
    so the handler [z] is dead and no proof obligation bears on it. *)

  (* TODO Comment on generalization *)
  Lemma pure_try2 `{Observe A1 V1} `{Observe A2 V2} {E1 E2}
    (m : micro V1 _) (h : outcome2 V1 E1 -> micro V2 E2)
    (φ : A2 -> _) (φ' : A1 -> _) ψ (ψ' : E1 -> Prop):
    pure m φ' ψ' →
    (∀ a, φ' a → pure (continue h ♯a) φ ψ) →
    (∀ a, ψ' a → pure (discontinue h a) φ ψ) →
    pure (V := V2) (try2 m h) φ ψ.
  Proof.
    intros; eapply pure_wp_try2_conseq; eauto;
    simpl; intros v Hv; eauto with pure.
  Qed.

  (* A reasoning rule for [try]; corollary of [pure_try2] *)

  Corollary pure_try `{Observe A1 V1} `{Observe A2 V2} {E}
    (m : micro V1 _) k z (φ : A2 -> _) (φ' : A1 -> _)  ψ :
    pure m φ' ψ →
    (∀ a, φ' a → pure (k ♯a) φ ψ) →
    (∀ a, ψ a → pure (z a) φ ψ) →
    pure (E := E) (V := V2) (try m k z) φ ψ.
  Proof.
    intros; eapply pure_try2; eauto.
  Qed.

  (* A reasoning rule for [bind]. *)

  (* Without [Observe], this bind type was restricted (it would be [@bind val val]).
    Attempting to apply this lemma to [@bind A1 A2] where [A1] and [A2] are types other
    than [val] did not work. Thanks to [Observe], now we can vary the Value types
    and the injection from [A1] and [A2]. *)

  (* TODO Comment on generalization *)
  Lemma pure_bind `{Observe A1 V1} `{Observe A2 V2} {E}
    m (k : V1 -> micro V2 _) (φ : A2 -> _) (φ' : A1 -> _) ψ :
    pure (V := V1) (E := E) m φ' ψ →
    (∀ a, φ' a → pure (V := V2) (k ♯a) φ ψ) →
    pure (V := V2) (bind m k) φ ψ.
  Proof.
    rewrite bind_as_try.
    intros; eapply pure_try; intros; eauto with pure.
  Qed.

  Lemma pure_strong_bind `{Observe A1 V1} `{Observe A2 V2} {E}
    m (k : V1 -> micro V2 _)
    (φ : A2 -> _) (φ' : A1 -> _) ψ ψ' :
    pure (E := E) m φ' ψ' →
    (∀ a, φ' a → pure (k ♯ a) φ ψ) →
    (∀ a, ψ' a → ψ a) →
    pure (bind m k) φ ψ.
  Proof.
    rewrite bind_as_try.
    intros; eapply pure_try; intros; eauto with pure.
    eapply pure_wp_mono; eauto.
  Qed.

  Lemma pure_bind_unary `{Observe A V} {E}
    m (k : V -> micro V _) (φ : A -> Prop) ψ :
    pure m (λ (a : A), pure (k ♯ a) φ ψ) ⊥ →
    pure (E := E) (bind m k) φ ψ.
  Proof.
    intros; eapply pure_strong_bind; eauto; done.
  Qed.

  (* We cannot give a reasoning rule for [par m1 m2] because its type is
    [micro (val * val)], not [micro val]. However, we can give a rule
    for [Par m1 m2 k z] if [k] transforms [val * val] into [val]. *)

  (* A reasoning rule for [Par m1 m2 k z]. *)

  (* TODO Comment on generalization *)
  Lemma pure_Par
    `{Observe A1 V1, Observe A2 V2, Observe A3 V3} {E E'}
    (m1 : micro V1 _) (m2 : micro V2 _)
    (φ : A3 -> Prop) ψ
    (k : outcome2 (V1 * V2) E' → micro V3 E) :
    pure m1
      (λ a1 : A1,
        pure m2
          (λ a2 : A2,
              pure (continue k (♯ a1, ♯ a2)) φ ψ)
          (λ e, pure (discontinue k e) φ ψ))
      (λ e, pure (discontinue k e) φ ψ) →
    pure m2
      (λ a2 : A2,
        pure m1
          (λ a1 : A1,
              pure (continue k (♯ a1, ♯ a2)) φ ψ)
          (λ e, pure (discontinue k e) φ ψ))
      (λ e, pure (discontinue k e) φ ψ) →
    pure (Par m1 m2 k) φ ψ.
  Proof.
    intros; eapply pure_wp_Par;
      do 2 (eapply pure_wp_mono; eauto;
      intros; returns_eauto; eauto).
  Qed.

  (* Sequentializations of previous lemmas, considering the LHS first *)

  Lemma pure_par_seq_strong
    `{Observe A1 V1, Observe A2 V2, Observe A3 V3} {E E'}
    (m1 : micro V1 _) (m2 : micro V2 _) (k : _ -> micro V3 E')
    (φ : A3 → Prop) ψ
  :
    pure (E := E) m1 (λ a1 : A1,
      pure m2 (λ a2 : A2,
        pure (continue k (♯ a1, ♯ a2)) φ ψ) ⊥) ⊥ →
    pure (E := E') (Par m1 m2 k) φ ψ.
  Proof.
    intros Hm1.
    apply pure_wp_Par_vals_left.
    eapply (pure_wp_mono_ret _ Hm1); intros ? (a1 & -> & Hm2).
    eapply (pure_wp_mono_ret _ Hm2); intros ? (a2 & -> & Hk).
    eauto.
  Qed.

  Lemma pure_par_seq
    `{Observe A1 V1, Observe A2 V2, Observe A3 V3} {E E'}
    (m1 : micro V1 _) (m2 : micro V2 _) (k : _ -> micro V3 E')
    (φ : A3 → Prop) ψ
  :
    pure (E := E) m1
      (λ a1,
        pure m2
          (λ a2, pure (continue k (♯ a1, ♯ a2)) φ ψ)
          (λ e : E, pure (discontinue k e) φ ψ))
      ⊥ →
    pure (E := E') (Par m1 m2 k) φ ψ.
  Proof.
    intros Hm1.
    apply pure_wp_Par_val_left.
    eapply (pure_wp_mono_ret _ Hm1); intros ? (a1 & -> & Hm2).
    eapply (pure_wp_mono_ret _ Hm2); intros ? (a2 & -> & Hk).
    eauto.
  Qed.

  Lemma pure_par_glue2
    `{Observe A1 V1, Observe A2 V2, Observe A3 V3} {E1 E2}
    (m1 : micro V1 E1) (m2 : micro V2 E1) (k : V1 * V2 -> micro V3 E2)
    (φ1 : A1 → Prop) (φ2 : A2 → Prop) (φ : A3 → Prop)
    (ψ : E1 -> Prop) (ψ' : E2 -> Prop) (z : E1 -> micro V3 E2) :
    pure (E := E1) m1 φ1 ψ ->
    pure (E := E1) m2 φ2 ψ ->
    (∀ (a1 : A1) (a2 : A2),
        φ1 a1 → φ2 a2 →
        pure (V := V3) (k (♯ a1, ♯ a2)) φ ψ') ->
    (∀ e, ψ e → pure (z e) φ ψ') ->
    pure (E := E2) (Par m1 m2 (glue2 k z)) φ ψ'.
  Proof.
    intros Hm1 Hm2 Hentail1 Hentail2. rewrite <- try_par.
    eapply pure_wp_try_conseq; eauto.
    { eapply pure_wp_par with (φ := λ v, pure (k v) φ ψ');
        [ eauto | eauto |].
      simpl. intros v1 v2 ? ?. eauto with pure. }
    { intros v. tauto. }
  Qed.

  Lemma pure_par_cont
    `{Observe A1 V1, Observe A2 V2, Observe A3 V3} {E1 E2}
    m1 m2 k
    (φ1 : A1 → Prop) (φ2 : A2 → Prop) (φ : A1 * A2 → Prop)
    (ψ : E1 -> Prop) (ψ' : E2 -> Prop):
    pure (E := E1) m1 φ1 ψ →
    pure m2 φ2 ψ →
    (∀ a1 a2, φ1 a1 → φ2 a2 → pure (continue k (♯ a1, ♯ a2)) φ ψ') →
    (forall e, ψ e -> pure (discontinue k e) φ ψ') ->
    pure (E := E2) (Par m1 m2 k) φ ψ'.
  Proof.
    intros. eapply pure_wp_Par_conseq; eauto; firstorder subst; eauto.
  Qed.

  (* A reasoning rule for [choose]. *)

  Lemma pure_choose `{Observe A V} m1 m2 (φ : A → Prop) (ψ : exn -> Prop) :
    pure (V := V) m1 φ ψ →
    pure m2 φ ψ →
    pure (choose m1 m2) φ ψ.
  Proof.
    apply pure_wp_choose.
  Qed.

  (* Inversion lemmas on [pure]. *)

  Lemma invert_pure_ret `{Observe A V} {E}
    (v : V) (φ : A → Prop) ψ :
    pure (E := E) (ret v) φ ψ →
    returns φ v.
  Proof.
    intros Hv; by apply invert_pure_wp_ret in Hv.
  Qed.

  Lemma invert_pure_throw `{Observe A V} {E}
    (e : E) (φ : A → Prop) ψ :
    pure (E := E) (throw e) φ ψ →
    ψ e.
  Proof.
    intros Hv; by apply invert_pure_wp_throw in Hv.
  Qed.

  (* This is the reciprocal bind rule for [pure_wp]. *)

  (* Because [pure_wp m ##_ ⊥] requires the result of [m] to lie in the image of the
    function [encode], and because this image cannot include every inhabitant
    of the type [val], we cannot expect that [pure_wp (bind m k) ##φ ⊥] implies
    [pure_wp m ##_ ⊥]. Thus, we can establish the reciprocal bind rule only under
    the side condition [pure_wp m ##(λ a, True) ⊥], which means that the result of
    the computation [m] lies in the image of the function [encode] at type
    [A]. *)

  Lemma invert_pure_bind `{Observe A V} {E}
    m k (φ : A → Prop) ψ :
    pure (V := V) (bind m k) φ ψ →
    pure m (λ (a : A), True) ψ →
    pure (V := V) m (λ (a : A), pure (E := E) (k ♯a) φ ψ) ψ.
  Proof.
    intros Hmk%invert_pure_wp_bind Hm.
    pose proof pure_wp_binary_intersection _ Hmk Hm as I.
    eapply (pure_wp_mono _ I); firstorder subst; eauto.
  Qed.

  (* That said, if we take the type [A] to be [val], then -- because [encode]
    at type [val] is the identity function -- this side condition becomes
    trivial, and we can prove a version of the rule that does not have this
    side condition. *)

  Lemma invert_pure_bind_unary `{Observe A V} {E}
    m k (φ : A → Prop) ψ:
    pure (V := V) (E := E) (bind m k) φ ψ →
    pure m (λ (v : val), pure (k v) φ ψ) ψ.
  Proof.
    intros Hmk%invert_pure_wp_bind.
    eapply pure_wp_mono; eauto; intros.
    eauto with pure.
  Qed.

  (* -------------------------------------------------------------------------- *)

  (* Because the relation [pure_wp] is inductively defined, the [pure_wp] judgement *)
(*     implies that [m] terminates. This forms a Hoare logic of pure correctness *)
(*     for pure_wp computations. *)

  (* This file offers lemmas and tactics that help work with [pure_wp] goals. *)
(*     These lemmas and tactics form a simple "proof mode" for pure_wp *)
(*     computations. *)

  (* -------------------------------------------------------------------------- *)
  (* TODO Comment on generalization *)
  Lemma pure_prove_bind_bind `{Observe A1 V1, Observe A2 V2} {E}
    (m : micro A1 E) (a : A2)
    (f : A1 -> micro V2 E) (g : V2 -> micro V2 E)
    (φ : A2 -> Prop) ψ:
    pure ('c ← m; f c) (singleton a) ψ ->
    pure (g ♯ a) φ ψ ->
    pure (E := E)
         ('v1 ← m;
          'v2 ← f v1;
          g v2) φ ψ.
  Proof.
    intros Hfm Hga.
    apply invert_pure_wp_bind in Hfm.
    eapply pure_wp_bind_conseq; eauto. simpl.
    intros a' Ha'.
    eapply pure_wp_bind_conseq; eauto.
    by intros _v (_a & -> & ->).
  Qed.

  Lemma pure_bind_bind `{Observe A V} {E}
    (m : micro A E)
    (f : A -> micro V E)
    (g : V -> micro V E)
    (φ : A -> Prop) ψ :
    pure (E := E)
      ('x ← m; f x) φ ψ ->
    (forall y, φ y -> pure (g ♯ y) φ ψ) ->
    pure ('v1 ← m;
          v2 ← f v1;
          g v2) φ ψ.
  Proof.
    intros Hm Hga.
    apply invert_pure_wp_bind in Hm.
    eapply pure_wp_bind_conseq; eauto. simpl.
    intros v Hv.
    eapply pure_wp_bind_conseq; eauto. simpl.
    intros _ (y & -> & Hy).
    apply Hga, Hy.
  Qed.

End pure_rules.

(* LATER: See if generalization to [Observe] is useful. *)
Section pure_rules_variant.

  (* When a [val] is returned, there is no need for [encode]. *)
  Lemma pure_ret_val `{Encode A} {E} (a : val) (ϕ : A -> Prop) ψ :
    returns ϕ a ->
    pure (E := E) (ret a) ϕ ψ.
  Proof.
    intros. returns_eauto. by eapply pure_ret.
  Qed.

  Lemma pure_ret_eq_val {E} (a : val) ψ :
    pure (E := E) (ret a) (λ a', a' = a) ψ.
  Proof.
    intros; eapply pure_ret; eauto.
  Qed.

End pure_rules_variant.


(* Rules about [pure] related to effectful computations, i.e. computations
   using [Stop] *)
Section pure_eff.

  (* The following two lemmas paraphrase the definition of [call] in eval.v.
      When applied to a goal of the form [simp (call v1 v2) _] where [v1] is
      a concrete closure (as opposed to a rigid metavariable), they step into
      the call. *)

  Lemma pure_enter_call_VClo `{Encode Y} η a v2 (φ : Y → Prop) ψ :
    pure (acall η a v2) φ ψ ->
    pure (call (VClo η a) v2) φ ψ.
  Proof.
    tauto.
  Qed.

  Lemma pure_stop_eval {Y} `{Encode X} η e k (φ : X -> Prop) ψ :
    pure (try2 (eval η e) k) φ ψ ->
    pure (V := val) (E := Y) (Stop CEval (η, e) k) φ ψ.
  Proof.
    intros.
    eapply pure_wp_simp; [ apply simplification.SimpEval | assumption ].
  Qed.

  (** [CEval] evaluation *)

  Lemma pure_CEval `{Encode A} {E η e k} (φ : A → Prop) (ψ : E → Prop) :
    pure (try2 (eval η e) k) φ ψ →
    pure (V := val) (Stop CEval (η, e) k) φ ψ.
  Proof.
    intros. eapply pure_wp_det_may_backward; eauto. repeat constructor.
    by intros m' ->%pure.invert_may_eval.
  Qed.

  Lemma pure_CEval_inject2 `{Encode A} {η e φ ψ} :
    pure (A := A) (eval η e) φ ψ →
    pure (Stop CEval (η, e) inject2) φ ψ.
  Proof.
    intros He. eapply pure_CEval, pure_try2; try done;
    intros; eauto using pure_ret, pure_throw.
  Qed.

  Lemma pure_enter_call_VCloRec `{Encode Y} η rbs g x e v2 (φ : Y → Prop) ψ :
    lookup_rec_bindings rbs g = ret (AnonFun x e) ->
    pure (eval ((x, v2) :: eval_rec_bindings η rbs ++ η) e) φ ψ ->
    pure (call (VCloRec η rbs g) v2) φ ψ.
  Proof.
    intros Hlookup Hpure.
    simpl; rewrite Hlookup.
    cbn.
    eapply pure_CEval; rewrite try2_ret_right.
    done.
  Qed.

  Lemma invert_pure_call `{Encode Y} f v (φ : Y -> Prop) :
    pure (call f v) φ ⊥ ->
    (exists η a, f = VClo η a) \/ exists η rbs g, f = VCloRec η rbs g.
  Proof.
    intros Hcall.
    unfold call in Hcall.
    destruct f; simpl in Hcall;
      ((exfalso; by eapply invert_pure_wp_crash) || eauto).
  Qed.

  (** Compatibility with [widen] *)

  Lemma pure_widen `{Encode A} {E} (m : micro val void) φ ψ :
    pure m φ ⊥ → @pure A _ val _ E (widen m) φ ψ.
  Proof.
    unfold widen.
    intros P. eapply pure_try2. eapply P.
    - intros. cbn. eapply pure_ret; done.
    - by intros.
  Qed.

End pure_eff.


