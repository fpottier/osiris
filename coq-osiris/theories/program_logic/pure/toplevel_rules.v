From osiris Require Import base.
From osiris.lang Require Import syntax encode sugar locations.
From osiris.semantics Require Import semantics.

From osiris.program_logic.pure Require Import
  pure_rules pattern_rules binding_rules call_rules expr_rules.

(* This file defines contains reasoning rules about top-level definitions,
   such as struct items, bindings, and modules. *)

(* -------------------------------------------------------------------------- *)

(* A judgement for the evaluation of structure items. *)

Definition struct_item ηδ item (φ : envs -> Prop) :=
  pure_wp (eval_sitem ηδ item) φ (λ _, False).

Definition struct_items ηδ sitems (φ : envs -> Prop) :=
  pure_wp (eval_sitems ηδ sitems) φ (λ _, False).


(* A judgement for the evaluation of module expressions. *)

Definition eval_module η me (φ : env -> Prop) :=
  pure_wp (eval_mexpr η me) (λ v, match v with
                                 | VStruct η' => φ η'
                                 | _ => False
                                 end) (λ _, False).


(* A judgement for module coercion. *)

Definition coerces c η (φ : env -> Prop) :=
  pure_wp (coerce c (VStruct η)) (λ v, match v with
                                       | VStruct η' => φ η'
                                       | _ => False
                                       end) (λ _, False).


(* -------------------------------------------------------------------------- *)

Lemma struct_consequence ηδ item (φ φ' : envs -> Prop) :
  struct_item ηδ item φ ->
  (∀ ηδ, φ ηδ -> φ' ηδ) ->
  struct_item ηδ item φ'.
Proof.
  unfold struct_item. eauto using pure_wp_mono.
Qed.

(* -------------------------------------------------------------------------- *)

(* Syntax-directed reasoning rules for the auxiliary judgement [structs]. *)

Lemma structs_nil ηδ (φ : envs -> Prop) :
  φ ηδ ->
  struct_items ηδ [] φ.
Proof.
  unfold struct_items.
  simpl_eval_sitems.
  by apply pure_wp_ret.
Qed.

Lemma structs_cons_unary ηδ item items (φ : envs -> Prop) :
  struct_item ηδ item (λ ηδ', struct_items ηδ' items φ) ->
  struct_items ηδ (item :: items) φ.
Proof.
  unfold struct_items, struct_item. intros.
  simpl_eval_sitems.
  apply pure_wp_bind.
  eapply pure_wp_mono; eauto.
Qed.

Lemma structs_cons ηδ item items φ ψ :
  struct_item ηδ item ψ ->
  (∀ ηδ', ψ ηδ' -> struct_items ηδ' items φ) ->
  struct_items ηδ (item :: items) φ.
Proof.
  unfold struct_items, struct_item. intros.
  simpl_eval_sitems.
  eapply pure_wp_bind_conseq; eauto.
Qed.

(* Syntax-directed reasoning rules for the judgement [struct]. *)

Lemma struct_let η δ bs (φ : envs -> Prop) ψ :
  bindings η bs ψ ⊥ ->
  (∀ η', ψ η' -> φ (η' ++ η, η' ++ δ)) ->
  struct_item (η, δ) (ILet bs) φ.
Proof.
  unfold struct_item; simpl_eval_sitem; intros.
  eapply pure_wp_bind_conseq; eauto using pure_wp_ret.
Qed.

Lemma unfold_struct_letrec η δ rbs (φ : envs -> Prop) ψ :
  ψ (eval_rec_bindings η rbs) ->
  (∀ η', ψ η' -> φ (η' ++ η, η' ++ δ)) ->
  struct_item (η, δ) (ILetRec rbs) φ.
Proof.
  unfold struct_item; intros.
  simpl_eval_sitem; auto using pure_wp_ret.
Qed.

Lemma struct_let_single η δ e name (spec : val -> Prop) :
  pure (eval η e) spec ⊥ ->
  struct_item (η, δ) (ILet [Binding (PVar name) e])
    (λ '(η0, δ0),
      ∃ clo, spec clo /\
                   η0 = [(name, clo)] ++ η /\
                   δ0 = [(name, clo)] ++ δ) .
Proof.
  intros; unfold struct_item; simpl_eval_sitem; simpl_eval_bindings.
  eapply pure_wp_simp. { simpl. apply SimpParRetRight. }
  eapply pure_wp_try2_conseq; eauto. 2: intros _ [].
  simpl; intros ? (? & -> & Hextend).
  unfold irrefutably_extend; simpl_eval_pat.
  eapply pure_wp_ret; eauto.
Qed.

Lemma struct_let_pat η δ p e (spec : val -> Prop) (φ : envs -> Prop) ψ :
  pure (eval η e) (λ v, pattern η [] p v ψ False) ⊥ ->
  (∀ η', ψ η' -> φ (η' ++ η, η' ++ δ)) ->
  struct_item (η, δ) (ILet [Binding p e]) φ.
Proof.
  intros; unfold struct_item; simpl_eval_sitem; simpl_eval_bindings.
  eapply pure_wp_simp. { simpl. apply SimpParRetRight. }
  eapply pure_wp_try2_conseq; eauto. 2: intros _ [].
  simpl; intros v (? & -> & Hextend).
  eapply pure_wp_bind_conseq.
  { unfold irrefutably_extend; cbn.
    apply pure_wp_widen.
    eapply pure_wp_try_conseq; eauto.
    2: intros _ [].
    eauto using pure_wp_ret. }
  auto using pure_wp_ret.
Qed.

Lemma struct_letrec_single η δ name af (spec : val -> Prop) :
  spec (VCloRec η [RecBinding name af] name) ->
  struct_item (η, δ)
    (ILetRec [RecBinding name af])
    (λ '(η0, δ0),
      ∃ clo, spec clo /\
               η0 = [(name, clo)] ++ η /\
               δ0 = [(name, clo)] ++ δ) .
Proof.
  intros; unfold struct_item.
  simpl_eval_sitem.
  apply pure_wp_ret; eauto.
Qed.

Lemma struct_module η δ m me (φ : envs -> Prop) (φ' : env -> Prop) :
  eval_module η me φ' ->
  (∀ η', φ' η' -> φ ((m, VStruct η') :: η, (m, VStruct η') :: δ)) ->
  struct_item (η, δ) (IModule m me) φ.
Proof.
  unfold struct_item; intros.
  simpl_eval_sitem.
  eapply pure_wp_bind_conseq; eauto.
  intros []; try contradiction; eauto using pure_wp_ret.
Qed.

Lemma struct_open η δ me (φ : envs -> Prop) (φ' : env -> Prop) :
  eval_module η me φ' ->
  (∀ η', φ' η' -> φ (η' ++ η, δ)) ->
  struct_item (η, δ) (IOpen me) φ.
Proof.
  unfold struct_item; intros.
  simpl_eval_sitem.
  eapply pure_wp_bind_conseq.
  { unfold as_struct.
    eapply pure_wp_bind_conseq; eauto.
    intros [] Hx; try contradiction. apply pure_wp_widen;
    eauto using pure_wp_ret. }
  eauto using pure_wp_ret.
Qed.

Lemma struct_include η δ me (φ : envs -> Prop) module_spec :
  eval_module η me module_spec ->
  (∀ η', module_spec η' ->
         φ (η' ++ η, η' ++ δ)) ->
  struct_item (η, δ) (IInclude me) φ.
Proof.
  unfold struct_item; intros.
  simpl_eval_sitem.
  eapply pure_wp_bind_conseq.
  { unfold as_struct.
    eapply pure_wp_bind_conseq; eauto.
    intros [] Hx; try contradiction; apply pure_wp_widen;
      eauto using pure_wp_ret. }
  eauto using pure_wp_ret.
Qed.

(* Rec *)

Lemma struct_letrec_function `{Encode X, Encode Y}
  `{WF_x : WellFounded X}
  η δ f bs
  (φf : X -> Y -> Prop)
  (Ψ : envs -> Prop)
  (R : X -> X -> Prop)
  (P : X -> Prop)
  ζ :
  (∀ vf x,
    (∀ y,
        wf_relation (WF := WF_x) y x ->
        R y x ->
        P y ->
        pure (call vf #y) (φf y) ζ) ->
      P x ->
      branches (("__osiris_anonymous_arg", #x) :: (f, vf) :: η) (O3Ret #x) bs (φf x) ζ) ->
  (∀ vf, (∀ x, P x -> pure (call vf #x) (φf x) ζ) -> Ψ ((f, vf) :: η, (f, vf) :: δ)) ->
  struct_item (η, δ) (ILetRec [RecBinding f (AnonFunction bs)]) Ψ.
Proof.
  intros Hvf Hnext.
  eapply unfold_struct_letrec. cbn.
  apply eq_refl.
  intros ? <-. eapply Hnext.
  intros x HP.
  eapply pure_rec_call; eauto.
  intros ????.
  eapply pure_eval_match. { eapply pure_eval_path; eapply pure_ret; encode. }
  eauto.
Qed.

Lemma struct_letrec2 `{Encode X, Encode Y, Encode W} `{WF_xy : WellFounded (X * Y)} (A : Type)
  η δ f arg1 arg2 e
  (Ψ : envs -> Prop)
  (φf : A -> X -> Y -> W -> Prop)
  (R : (X * Y) -> (X * Y) -> Prop)
  (P : A -> X -> Y -> Prop)
  (ζ : A -> exn -> Prop) :
  (∀ vf x1 y1 a,
      (∀ x2 y2,
          wf_relation (WF := WF_xy) (x2, y2) (x1, y1) ->
          R (x2, y2) (x1, y1) ->
          P a x2 y2 ->
          pure_call2 vf #x2 #y2 (φf a x2 y2) (ζ a)) ->
      P a x1 y1 ->
      pure (eval ((arg2, #y1) :: (arg1, #x1) :: (f, vf) :: η) e)
        (φf a x1 y1) (ζ a)) ->
  (∀ vf,
      (∀ a x y, P a x y -> pure_call2 vf #x #y (φf a x y) (ζ a)) ->
      Ψ ((f, vf) :: η, (f, vf) :: δ)) ->
  struct_item (η, δ) (ILetRec [RecBinding f (AnonFun arg1 (EAnonFun (AnonFun arg2 e)))]) Ψ.
Proof.
  intros Hvf Hnext.
  eapply unfold_struct_letrec. cbn.
  apply eq_refl.
  intros ? <-. eapply Hnext.
  intros a x y HP.
  eapply pure_rec_call2; eauto.
  intros ?????.
  eapply pure_eval_anonfun. simpl. apply pure_stop_eval.
  rewrite try2_ret_right.
  eauto.
Qed.

Lemma struct_letrec `{Encode X, Encode Y} `{WF_x : WellFounded X}
  η δ f arg e1
  (φf : X -> Y -> Prop)
  (Ψ : envs -> Prop)
  (R : X -> X -> Prop)
  (P : X -> Prop) ζ :
  (∀ vf x,
      (∀ y,
          wf_relation (WF := WF_x) y x ->
          R y x ->
          P y ->
          pure (call vf #y) (φf y) ζ) ->
      P x ->
      pure (eval ((arg, #x) :: (f, vf) :: η) e1) (φf x) ζ) ->
  (∀ vf, (∀ x, P x -> pure (call vf #x) (φf x) ζ) -> Ψ ((f, vf) :: η, (f, vf) :: δ)) ->
  struct_item (η, δ) (ILetRec [RecBinding f (AnonFun arg e1)]) Ψ.
Proof.
  intros Hvf Hnext.
  eapply unfold_struct_letrec. cbn.
  apply eq_refl.
  intros ? <-. eapply Hnext.
  intros x HP.
  eapply pure_rec_call; eauto.
Qed.


(* -------------------------------------------------------------------------- *)

(* Syntax-directed reasoning rules for the auxiliary judgement [module]. *)

#[export]
  Hint Extern 1 (returns _ _) => unfold returns; by firstorder : pure_wp.


Lemma pure_wp_module η me φ :
  eval_module η me φ ->
  pure (eval_mexpr η me) (λ v, match v with
                               | VStruct η => φ η
                               | _ => False
                               end) ⊥.
Proof.
  repeat intro.
  eapply pure_wp_mono; intros; eauto with pure_wp.
Qed.

Lemma pure_eval_mexpr_struct (η δ whatenv : env) items (ψ : val -> Prop) :
  simp (eval_sitems (η, []) items) (ret (whatenv, δ)) ->
  ψ (VStruct δ) ->
  pure (eval_mexpr η (MStruct items)) ψ ⊥.
Proof.
  intros. simpl_eval_mexpr.
  eapply pure_wp_simp; [ simp | eauto using pure_wp_ret with pure ].
Qed.

(* Lemma pure_eval_mexpr_coerc η me c v (ψ : val -> Prop): *)
(*   simp (eval_mexpr η me) (ret v) -> *)
(*   pure (coerce c v) ψ ⊥ -> *)
(*   pure (eval_mexpr η (MCoercion me c)) ψ ⊥. *)
(* Proof. *)
(*   intros Hme Hc. simpl_eval_mexpr. eapply pure_bind. *)
(*   eapply pure_wp_simp; eauto. *)
(*   eapply pure_wp_mono_ret. eapply pure_wp_ret_eq. *)
(*   intros _ ->. eauto with pure *)
(*   rewrite pure_wp_widen'. *)
(*   apply Hc. *)
(* Qed. *)

Lemma module_struct η sitems φ :
  struct_items (η, []) sitems (λ '(η, δ), φ δ) ->
  eval_module η (MStruct sitems) φ.
Proof.
  intros; unfold eval_module.
  simpl_eval_mexpr.
  eapply pure_wp_bind_conseq; [ eassumption | ].
  intros [??]; auto using pure_wp_ret.
Qed.

Lemma module_struct_let η bs sitems φ ψ :
  bindings η bs ψ ⊥ ->
  (∀ η', ψ η' ->
         struct_items (η' ++ η, η') sitems (λ '(_, δ), φ  δ)) ->
  eval_module η (MStruct ((ILet bs) :: sitems)) φ.
Proof.
  intros Hbs Hψsitems.
  eapply module_struct.
  { eapply structs_cons_unary.
    eapply struct_let; [ eassumption | ].
    intros; rewrite app_nil_r; by apply Hψsitems. }
Qed.

Lemma module_path η π φ :
  pure_wp (lookup_path η π) (λ v, match v with
                                 | VStruct η => φ η
                                 | _ => False
                                 end) (λ _, False) ->
  eval_module η (MPath π) φ.
Proof.
  unfold eval_module; simpl_eval_mexpr; intros.
  apply pure_wp_widen.
  eapply pure_wp_mono; eauto.
Qed.

Lemma module_coercion η me c φ :
  eval_module η me φ ->
  (∀ η', φ η' -> coerces c η' φ) ->
  eval_module η (MCoercion me c) φ.
Proof.
  intros; unfold eval_module.
  simpl_eval_mexpr.
  eapply pure_wp_bind_conseq; [ eassumption | ].
  intros [] Hx; try contradiction; apply pure_wp_widen;
    unfold coerces in *; auto.
Qed.
