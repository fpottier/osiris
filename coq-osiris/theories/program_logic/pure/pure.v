(* This file summarizes the content of the pure/ subdirectory. *)

From osiris.program_logic.pure Require Export
     judgements
     pure_rules
     call_rules
     expr_rules
     pattern_rules
     toplevel_rules
     fun_spec
     .
