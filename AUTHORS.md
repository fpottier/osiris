The following people, listed in alphabetical order, have contributed
to this project:

+ Arnaud Daby-Seesaram
+ Jean-Marie Madiot
+ François Pottier
+ Remy Seassau
+ Irene Yoon

Files from coq-osiris/theories/CompCert have their own author information with
results added by F. Pottier and R. Seassau, marked as "added for osiris".

External files in coq-osiris/theories/stdlib/src have their own authors
information.
